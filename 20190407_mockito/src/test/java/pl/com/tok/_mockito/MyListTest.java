
package pl.com.tok._mockito;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import org.mockito.Mockito;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

class CustonAnswer implements Answer<Boolean>{

    @Override
    public Boolean answer(InvocationOnMock invocation) throws Throwable {
       return true;
    }
    
}

public class MyListTest {
    
    public MyListTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
 
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGet() {
        MyList mockList = Mockito.mock(MyList.class, "my mock nr 1");
        when(mockList.get(anyInt())).thenReturn("X");    
        String sample = "Y";
        mockList.add(sample);
        mockList.add("Z");
        verify(mockList,atLeastOnce()).add(anyString());
        verify(mockList,atLeast(0)).add(anyString());
        verify(mockList,times(2)).add(anyString());
        assertEquals("X",mockList.get(0));
    }
    
    @Test
    public void testGet_1() {
        MyList mockList = Mockito.mock(MyList.class, new CustonAnswer());  
        String sample = "Y";
        boolean added = mockList.add(sample);  
        assertTrue(added);
    }

    @Test
    public void testSize() {
        MyList mockList = Mockito.mock(MyList.class);
        when(mockList.size()).thenReturn(5); 
        assertEquals(5, mockList.size());
    }
}
