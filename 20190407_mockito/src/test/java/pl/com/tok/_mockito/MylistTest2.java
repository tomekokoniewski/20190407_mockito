package pl.com.tok._mockito;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)

public class MylistTest2 {
    
    public MylistTest2() {
    }
        
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this); //inicjowanie mokowania przed każdym testem - konieczne !
        //jest to tożsame z: @RunWith(MockitoJUnitRunner.class) - mozemy dać albo jedno lub drugie
    }
    
    @After
    public void tearDown() {
    }
    
    @Mock
    MyList mockedList;
    
    @Spy
    List<String> spiedList = new ArrayList<>();
    
    @Captor
    ArgumentCaptor<String> argCaptor;
    
    @Test
    public void testListSize(){
        mockedList.add("x");
        verify(mockedList).add(anyString());
        when(mockedList.size()).thenReturn(5);
        assertEquals(5,mockedList.size());
    }
    
    @Test
    public void testSpiedList(){
        assertEquals(0,spiedList.size());
        spiedList.add("a");
        spiedList.add("b");
        verify(spiedList).add("a");
        assertEquals(2,spiedList.size());
    }
    
    @Test
    public void testCaptor(){
        mockedList.add("a");
        mockedList.add("b");
        verify(mockedList, atLeastOnce()).add(argCaptor.capture());
        assertEquals("b",argCaptor.getValue()); //argCaptor przechwyca ostatni obiekt,
        //więc sprawdzamy czy "b"
        assertEquals("a",argCaptor.getAllValues().get(0)); //pobraliśmy wszystkie wart i spr pierwszą
        
        
    }
}
