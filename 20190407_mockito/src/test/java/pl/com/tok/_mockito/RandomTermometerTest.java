package pl.com.tok._mockito;

import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

public class RandomTermometerTest {

    public RandomTermometerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetTemperature_0() {
        Random random = Mockito.mock(Random.class);
        when(random.nextDouble()).thenReturn(0.0);
        RandomTermometer termometer = new RandomTermometer();
        assertEquals(-50, termometer.getTemperature(random), 0.001);
    }

    @Test
    public void testGetTemperature_positive() {
        Random random = Mockito.mock(Random.class);
        when(random.nextDouble()).thenReturn(1.0);
        RandomTermometer termometer = new RandomTermometer();
        assertEquals(100, termometer.getTemperature(random), 0.001);
    }

    @Test
    public void testGetTemperature_negative() {
        Random random = Mockito.mock(Random.class);
        when(random.nextDouble()).thenReturn(-1.0);
        RandomTermometer termometer = new RandomTermometer();
        assertEquals(-200, termometer.getTemperature(random), 0.001);
    }

}
