package pl.com.tok._mockito;

import java.util.Random;

public class RandomTermometer {

    public double getTemperature(Random random) {
        if (random == null) {
            random = new Random();
        }
        return random.nextDouble() * 150 - 50;
    }

}
